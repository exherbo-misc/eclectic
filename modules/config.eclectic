# Copyright 1999-2005 Gentoo Foundation
# Copyright 2010-2011 Bo Ørsted Andresen
# Distributed under the terms of the GNU General Public License v2

inherit package-manager

DESCRIPTION="Handle configuration file merges"
MAINTAINER="ciaranm@exherbo.org"
SVN_DATE='$Date: 2006-08-01 18:35:34 -0400 (Tue, 01 Aug 2006) $'
VERSION=$(svn_date_to_version "${SVN_DATE}" )

show_extra_help_text() {
    cat <<ENDOFTEXT
This tool can be used to automatically or manually merge configuration files
which have been created by $(package-manager).

The 'interactive' action will enter interactive mode.

The 'list' action will show all configuration files which need updating.

The 'accept' action will accept the proposed changes to one or more
configuration files. It accepts one or more parameters, each of which can either
be the names of files to overwrite, a number corresponding to the value shown by
the 'list' action, or two numbers separated by a hyphen -- for example, "3-7" --
which is treated as a range.

The 'reject' action will discard (reject) the proposed changes to one or more
configuration files. It takes parameters in the same way as 'accept'.

The 'display' action shows (as a unified diff) the proposed update for one or
more files.

The 'accept-all' action accepts all proposed changes to all configuration files.
Similarly, 'reject-all' rejects all changes. Use with caution.

'accept-force' and 'accept-all-force' work like their counterparts without
the '-force' suffix, except that files are overwritten without asking.
ENDOFTEXT
}

config_protect_dirs() {
    if [[ -z ${CONFIG_UPDATE_DIRS} ]] ; then
        export CONFIG_UPDATE_DIRS=$(get_config_protect_dirs )
    fi
    echo ${CONFIG_UPDATE_DIRS}
}

find_targets() {
    local dir targets
    local OLD_IFS=${IFS}
    IFS=$'\n\t '
    for dir in $(config_protect_dirs ) ; do
        [[ -d ${dir} ]] || continue
        find ${dir} -iname '._cfg????_*' | \
            sed -e 's,^\(.*\)\._cfg...._\(.*\),\1\2,g' | sort -u
    done
    IFS=${OLD_IFS}
}

expand_range() {
    local x y=1 i=${1%-+([[:digit:]])} j=${1#+([[:digit:]])-}
    (( i >= j )) && y=-1
    for (( x=i ; x != j+y ; x+=y )) ; do
        echo -n " ${x}"
    done
}

generic_handle_one_file() {
    local action_name="${1}" apply_cmd="${2}"
    shift 2

    local targets=( ) has_targets=0

    # check whether we've been given any parameters
    if [[ ${#@} < 1 ]] ; then
        die -q "You should give me at least one file to work with"
    else
        local files_raw file files

        # for each parameter...
        for files_raw in "$@" ; do
            [[ -z ${files_raw} ]] && continue

            files=()
            # do we have a range, a number or a name?
            if [[ "${files_raw}" == +([[:digit:]])-+([[:digit:]]) ]] ; then
                # range
                files=( $(expand_range ${files_raw}) )
            elif is_number "${files_raw}" ; then
                # number
                files=( ${files_raw} )
            else
                # name. don't decode
                files=( "${files_raw}" )
            fi

            # do we have a number or a name?
            for file in "${files[@]}" ; do
                if is_number "${file}" ; then
                    # number. decode
                    if [[ ${has_targets} == 0 ]] ; then
                        local OLD_IFS=${IFS}
                        IFS=$'\n' targets=( $(find_targets ) )
                        IFS=${OLD_IFS}
                        has_targets=1
                    fi
                    file=${targets[${file} - 1]}
                else
                    file=${file}
                fi

                # sanity checks
                [[ -z ${file} ]] && die -q "Sorry, ${files_raw} seems to be invalid"
                [[ -e ${file} ]] || write_error_msg "${file} doesn't seem to exist"

                # find the files to discard
                local cfgname="._cfg????_$(basename "${file}")"
                local dirname="$(readlink -f $(dirname ${file} ))"
                local kfiles
                kfiles=( $(find "${dirname}" -maxdepth 1 -type f \
                    -iname "${cfgname}") )

                if [[ -n ${kfiles[@]//[[:space:]]} ]] ; then
                    # files found. work on them
                    ${apply_cmd} "${kfiles}"
                else
                    # no files found
                    write_error_msg "Couldn't find any files to ${action_name}."
                    write_error_msg "I was looking in: ${dirname}"
                    write_error_msg "For files named:  ${cfgname}"
                    die -q "Can't ${action_name} ${files_raw}"
                fi
            done
        done
    fi
}

generic_handle_all() {
    local verb="${1}" action="${2}"
    shift 2

    local targets target OLD_IFS=${IFS}
    write_list_start "${verb} changes to configuration files:"
    IFS=$'\n' targets=( $(find_targets ) )
    IFS=${OLD_IFS}
    if [[ ${#targets[@]} > 0 ]] ; then
        for target in ${targets[@]} ; do
            write_kv_list_entry "${target}" ""
            do_action config "${action}" "${target}"
        done
    else
        write_kv_list_entry "No out of date files found"
    fi
}

### list action ###

describe_list() {
    echo "List files which need merging"
}

describe_list_parameters() {
    echo "[--terse]"
}

describe_list_options() {
    echo "--terse : Only print the flat list of files"
}

do_list() {
    local targets OLD_IFS=${IFS}

    IFS=$'\n' targets=( $(find_targets ) )
    IFS=${OLD_IFS}

    if [[ ${1} == --terse ]]; then
        IFS=$'\n'
        [[ ${#targets[@]} -gt 0 ]] && targets+=( "" )
        echo -n "${targets[*]}"
        IFS=${OLD_IFS}
        return
    fi

    write_list_start "Configuration files needing action:"
    if [[ ${#targets[@]} > 0 ]] ; then
        write_numbered_list "${targets[@]}"
    else
        write_kv_list_entry "No out of date files found"
    fi
}

options_list() {
    :
}

### reject action ###

describe_reject() {
    echo "Reject updates to the specified files"
}

do_reject() {
    generic_handle_one_file "reject" "reject_handler" "$@"
}

reject_handler() {
    rm -i "$@"
}

options_reject() {
    find_targets
}

### accept action ###

describe_accept() {
    echo "Accept changes to the specified files"
}

do_accept() {
    MV_CMD="mv -i" generic_handle_one_file "accept" "accept_handler" "$@"
}

accept_handler() {
    local file
    for file in "$@" ; do
        local sfile="${file}" dfile="${file/._cfg????_}"
        echo "Replacing ${dfile} with ${sfile}..."
        if [[ ! -e ${dfile} ]]; then
            if [[ -L ${dfile} ]]; then
                die -q "Sorry, ${dfile} is a dangling symlink"
            else
                local response
                echo -n "${dfile} does not exist, replace with ${sfile}? "
                read response
                case ${response} in
                    y|Y|yes|YES|"") ;;
                    *) return 1     ;;
                esac
            fi
        fi
        ${MV_CMD:-mv -i} "${sfile}" "${dfile}"
    done
}

options_accept() {
    find_targets
}

### accept-force action ###

describe_accept-force() {
    echo "Accept changes to the specified files and overwrite them"
}

do_accept-force() {
    MV_CMD="mv -f" generic_handle_one_file "accept" "accept_handler" "$@"
}

options_accept-force() {
    find_targets
}

### reject-all action ###

describe_reject-all() {
    echo "Reject all updates ($(highlight_warning dangerous))"
}

do_reject-all() {
    generic_handle_all "Rejecting" "reject"
}

options_reject-all() {
    :
}

### accept-all action ###

describe_accept-all() {
    echo "Accept all updates ($(highlight_warning dangerous))"
}

do_accept-all() {
    generic_handle_all "Accepting" "accept"
}

options_accept-all() {
    :
}

### accept-all-force action ###

describe_accept-all-force() {
    echo "Accept all updates and force file overwrite ($(highlight_warning dangerous))"
}

do_accept-all-force() {
    generic_handle_all "Accepting" "accept-force"
}

options_accept-all-force() {
    :
}

### merge action ###

describe_merge() {
    echo "Interactively merge changes to the specified files"
}

do_merge() {
    generic_handle_one_file "merge" "merge_handler" "$@"
}

merge_handler() {
    local file
    for file in "$@" ; do
        local sfile="${file}" dfile="${file/._cfg????_}"
        echo "Merging ${dfile} with ${sfile}..."
        merge_prog ${dfile} ${sfile}

        echo "Clean up old file?"
        rm -i "${sfile}"
    done
}

merge_prog() {
    if [[ -n "${ECLECTIC_DIFF}" ]] ; then
        if type "${ECLECTIC_DIFF}" &>/dev/null ; then
            ${ECLECTIC_DIFF} "$@"
            return
        fi
    fi

    if type vimdiff &>/dev/null ; then
        vimdiff "$@"
    else
        write_error_msg "Couldn't find a suitable merge program."
        write_error_msg "Currently I know how to use 'vimdiff', which you can"
        write_error_msg "get by installing app-editors/vim."
        die -q "No merge program found"
    fi

}

options_merge() {
    find_targets
}

### merge-all action ###

describe_merge-all() {
    echo "Interactively merge all changes"
}

do_merge-all() {
    generic_handle_all "Merging" "merge"
}

options_merge-all() {
    :
}

### display action ###

describe_display() {
    echo "Display the proposed changes to the specified files"
}

do_display() {
    generic_handle_one_file "display" "display_handler" "$@"
}

display_handler() {
    local file
    for file in "$@" ; do
        local sfile="${file}" dfile="${file/._cfg????_}"
        echo "Proposed changes to ${dfile}:"
        [[ ! -e ${dfile} && ! -L ${dfile} ]] && dfile=/dev/null
        diff_prog -u ${dfile} ${sfile}
    done
}

diff_prog() {
    if [[ -n ${ECLECTIC_CONFIG_DIFF_COMMAND} ]]; then
        ${ECLECTIC_CONFIG_DIFF_COMMAND} "$@"
    elif type git &>/dev/null ; then
        git --no-pager --git-dir=: diff --color-words "$@"
    elif type colordiff &>/dev/null ; then
        colordiff "$@"
    else
        diff "$@"
    fi
}

options_display() {
    find_targets
}

### display-all action ###

describe_display-all() {
    echo "Display all proposed updates"
}

do_display-all() {
    generic_handle_all "Displaying" "display"
}

options_display-all() {
    :
}

### interactive action ###

describe_interactive() {
    echo "Enter interactive mode"
}

do_interactive() {
    local targets command show_menu=yes show_list=yes OLD_IFS=${IFS}

    config_protect_dirs 1>/dev/null
    while true ; do
        IFS=$'\n' targets=( $(find_targets) )
        IFS=${OLD_IFS}

        # Are we finished?
        if [[ ${#targets[@]} == 0 ]] ; then
            write_list_start "Nothing left to do."
            break
        fi

        # Should we show a file list?
        if [[ -n ${show_list} ]] ; then
            show_list=
            write_list_start "Configuration files needing action:"
            write_numbered_list "${targets[@]}"
            echo
        fi

        # Should we show a menu?
        if [[ -n ${show_menu} ]] ; then
            show_menu=
            write_list_start "Enter command:"
            write_kv_list_entry "<number>" "Work with the specified file"
            write_kv_list_entry "$(highlight d)isplay <number>" "Display proposed change"
            write_kv_list_entry "$(highlight a)ccept <number>"  "Accept proposed change"
            write_kv_list_entry "$(highlight r)eject <number>"  "Reject proposed change"
            write_kv_list_entry "$(highlight m)erge <number>"   "Merge proposed change"
            echo
            write_kv_list_entry "accept-all" \
                "Accept all changes ($(highlight_warning dangerous))"
            write_kv_list_entry "reject-all" \
                "Reject all changes ($(highlight_warning dangerous))"
            echo
            write_kv_list_entry "$(highlight l)ist" "Show file list"
            write_kv_list_entry "$(highlight h)elp" "Show this menu again"
            write_kv_list_entry "$(highlight q)uit" "Exit the program"
            echo
        else
            write_list_start "Enter command ($(highlight h)elp to show menu)"
        fi
        echo -n "> "
        read command || break

        case ${command%%[[:space:]]*} in
            q|qu|qui|quit)
                break;
                ;;
            h|he|hel|help)
                show_menu=yes
                ;;
            l|li|lis|list)
                show_list=yes
                ;;
            d|di|dis|disp|displ|displa|display)
                if [[ -z ${command//[^[:space:]]} ]] ; then
                    write_error_msg "You didn't tell me which file to display"
                else
                    echo do_action config display ${command#*[[:space:]]} >&2
                    do_action config display ${command#*[[:space:]]}
                fi
                ;;
            a|ac|acc|acce|accep|accept)
                if [[ -z ${command//[^[:space:]]} ]] ; then
                    write_error_msg "You didn't tell me which file to accept"
                else
                    do_action config accept ${command#*[[:space:]]}
                fi
                show_list=yes
                ;;
            r|re|rej|reje|rejec|reject)
                if [[ -z ${command//[^[:space:]]} ]] ; then
                    write_error_msg "You didn't tell me which file to reject"
                else
                    do_action config reject ${command#*[[:space:]]}
                fi
                show_list=yes
                ;;
            m|me|mer|merg|merge)
                if [[ -z ${command//[^[:space:]]} ]] ; then
                    write_error_msg "You didn't tell me which file to merge"
                else
                    do_action config merge ${command#*[[:space:]]}
                fi
                show_list=yes
                ;;
            accept-all)
                do_action config accept-all
                show_list=yes
                ;;
            accept-all-force)
                do_action config accept-all-force
                show_list=yes
                ;;
            reject-all)
                do_action config reject-all
                show_list=yes
                ;;
            *)
                local items valid=true
                for items in ${command} ; do
                    if [[ ${items} != +([[:digit:]]) &&
                       ${items} != +([[:digit:]])-+([[:digit:]]) ]] ; then
                       valid=false
                       break
                   fi
                done
                if ${valid} ; then
                    while true ; do
                        local item ok=
                        for items in ${command} ; do
                            [[ -z ${ok} ]] && write_list_start "Selected files:"
                            ok=yes
                            if ! is_number ${items} ; then
                                items=$(expand_range ${items})
                            fi
                            for item in ${items} ; do
                                [[ -z ${targets[${item}-1]} ]] && continue
                                write_numbered_list_entry ${item} ${targets[${item}-1]}
                            done
                        done
                        if [[ -z ${ok} ]] ; then
                            write_error_msg "Sorry, ${command} not valid"
                            show_list=yes
                            break
                        else
                            echo
                            write_list_start "Enter subcommand:"
                            write_kv_list_entry "$(highlight d)isplay" "Display proposed change"
                            write_kv_list_entry "$(highlight a)ccept"  "Accept proposed change"
                            write_kv_list_entry "accept-force"  "Accept proposed change and force file overwrite"
                            write_kv_list_entry "$(highlight r)eject"  "Reject proposed change"
                            write_kv_list_entry "$(highlight m)erge"   "Merge proposed change"
                            write_kv_list_entry "$(highlight q)uit"    "Return to parent menu"
                            echo
                            echo -n "> "
                            read subcommand
                            case ${subcommand} in
                                q|qu|qui|quit)
                                    break;
                                    ;;
                                d|di|dis|disp|displ|displa|display)
                                    do_action config display ${command}
                                    ;;
                                a|ac|acc|acce|accep|accept)
                                    do_action config accept ${command}
                                    show_list=yes
                                    break
                                    ;;
                                accept-force)
                                    do_action config accept-force ${command}
                                    show_list=yes
                                    break
                                    ;;
                                r|re|rej|reje|rejec|reject)
                                    do_action config reject ${command}
                                    show_list=yes
                                    break
                                    ;;
                                m|me|mer|merg|merge)
                                    do_action config merge ${command}
                                    show_list=yes
                                    break
                                    ;;
                                *)
                                    write_error_msg "Sorry, ${subcommand} not understood"
                                    ;;
                            esac
                        fi
                    done
                else
                    write_error_msg "Sorry, ${command} not understood"
                fi
                ;;
        esac
    done
}

options_interactive() {
    :
}

# vim: set ft=eclectic sw=4 sts=4 ts=4 et tw=80 :

