# vim: set et fdm=marker fmr={{{,}}} ft=eclectic sts=4 sw=4 ts=8:
# Copyright 2012 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'fontconfig.eclectic', which is:
#   Copyright 1997-2007 Gentoo Foundation

# {{{ metadata

DESCRIPTION="Manage fontconfig symlinks"
AUTHOR="Saleem Abdulrasool <compnerd@compnerd.org>"
VERSION="2.0"

# }}}

# {{{ support functions

: ${_FONTCONFIG_AVAILABLE_DIR:=${ROOT:-/}usr/share/fontconfig/conf.avail}
: ${_FONTCONFIG_ENABLED_DIR:=${ROOT:-/}etc/fonts/conf.d}

_available_options() {
    local -a options
    local option

    for option in "${_FONTCONFIG_AVAILABLE_DIR}"/*.conf ; do
        if [[ -f ${option} ]] ; then
            options+=( "${option##*/}" )
        fi
    done

    echo "${options[@]}"
}

_enabled() {
    [[ -e "${_FONTCONFIG_ENABLED_DIR}/${1}" ]]
}

# }}}

# {{{ list

describe_list() {
    echo "Lists all available fontconfig .conf files"
}

do_list() {
    local -a options
    local i

    options=( $(_available_options) )

    for (( i = 0 ; i < ${#options[@]} ; i++ )) ; do
        _enabled "${options[i]}" && options[i]="${options[i]} $(highlight '*')"
    done

    write_list_start "Available fontconfig .conf files:"
    if [[ ${#options[@]} -eq 0 ]] ; then
        write_kv_list_entry "(none found)" ""
    else
        write_numbered_list "${options[@]}"
    fi

    return 0
}

# }}}

# {{{ enable

describe_enable() {
    echo "Enable specified fontconfig .conf file(s)"
}

describe_enable_parameters() {
    echo "<target>"
}

describe_enable_options() {
    echo "<target>: target name or number (from 'list' action)"
}

do_enable() {
    local -a options
    local option

    [[ ${#} -eq 0 ]] && die -q "No fontconfig .conf file(s) specified"
    if [[ ! -w ${_FONTCONFIG_ENABLED_DIR} ]] ; then
        die -q "Insufficient permissions to ${_FONTCONFIG_ENABLED_DIR}"
    fi

    options=( $(_available_options) )

    for option in "${@}" ; do
        # convert numeric index into value if necessary
        if [[ ${1} == -+([[:digit:]]) ]] ; then
            [[ ${1#-} -gt ${#options} ]] && die -q "cannot find option '${1}'"
            option=${options[${1#-}-1]}
        fi

        case "${option}" in
        /*)     # absolute path
            option="${ROOT:-/}${option:1}"
            ;;
        */*)    # relative path
            option="$(realpath "${ROOT:-/}${PWD}/${option}")"
            ;;
        *)      # name
            if [[ -f ${option} ]] ; then
                option="$(realpath ${ROOT:-/}${PWD}/${option})"
            else
                option="${_FONTCONFIG_AVAILABLE_DIR}/${option}"
            fi
            ;;
        esac

        if [[ ! -e ${option} ]] ; then
            write_error_msg "'${option##*/}' does not exist"
            continue
        fi

        if [[ -e ${_FONTCONFIG_ENABLED_DIR}/${option##*/} ]] ; then
            write_error_msg "'${option##*/}' is already enabled"
            continue
        fi

        ln -s "${option}" "${_FONTCONFIG_ENABLED_DIR}" ||
            die -q "unable to create symlink from '${option}' to '${_FONTCONFIG_ENABLED_DIR}'"
    done

    return 0
}

# }}}

# {{{ disable

describe_disable() {
    echo "Disable specified fontconfig .conf file(s)"
}

describe_disable_parameters() {
    echo "<target>"
}

describe_disable_options() {
    echo "<target>: Target name or number (from 'list' action)"
}

do_disable() {
    local -a options
    local option

    [[ ${#} -eq 0 ]] && die -q "No fontconfig .conf file(s) specified"
    if [[ ! -w ${_FONTCONFIG_ENABLED_DIR} ]] ; then
        die -q "Insufficient permissions to ${_FONTCONFIG_ENABLED_DIR}"
    fi

    options=( $(_available_options) )

    for option in "${@}" ; do
        # convert numeric index into value if necessary
        if [[ ${1} == -+([[:digit:]]) ]] ; then
            [[ ${1#-} -gt ${#options} ]] && die -q "cannot find option '${1}'"
            option=${options[${1#-}-1]}
        fi

        if [[ ! -e ${_FONTCONFIG_ENABLED_DIR}/${option} ]] ; then
            write_error_msg "${option} is not enabled"
            continue
        fi

        rm "${_FONTCONFIG_ENABLED_DIR}/${option}" ||
            die -q "unable to remove '${option}'"
    done

    return 0
}

# }}}

